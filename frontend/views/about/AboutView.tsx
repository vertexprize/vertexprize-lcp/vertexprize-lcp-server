export default function AboutView() {
  return (
    <div className="flex flex-col h-full items-center justify-center p-l text-center box-border">
      <img style={{ width: '200px' }} src="images/empty-plant.png" />
      <h2>Список проектов</h2>
      <p>Отсутвуют проекты</p>
    </div>
  );
}
