package org.vertexprize.lcp.server.data;

public enum Role {
    USER, ADMIN;
}
