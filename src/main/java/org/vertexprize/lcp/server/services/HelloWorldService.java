package org.vertexprize.lcp.server.services;

import com.vaadin.flow.server.auth.AnonymousAllowed;
import dev.hilla.BrowserCallable;
import org.springframework.stereotype.Service;

@BrowserCallable
@AnonymousAllowed
@Service
public class HelloWorldService {

    public String sayHello(String name) {
        if (name.isEmpty()) {
            return "Ошибка: имя проекта не указано";
        } else {
            return "Создан новый проект" + name;
        }
    }
}
